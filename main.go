package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	fmt.Println("Fridge")
	f := Fridge{}
	fmt.Println(f.IsOpen())
	f.Shufle()
	f.Print()
	fmt.Println(f.IsOpen())

	solution, found := Solve(&f)
	if found {
		fmt.Println("Solution found:", solution)
	} else {
		fmt.Println("No solution exists.")
	}

	for _, turn := range solution {
		f.Turn(turn[1], turn[0])
	}
	fmt.Println(f.IsOpen())

	bitFridge()
}

func bitFridge() {
	fmt.Println("Bit fridge")
	f := FridgeBit{state: 0b1111111111111111} // Starting with all 1s for demonstration
	fmt.Println("Is open:", f.isSolved())

	f.turnBit(0, 0) // Flip a bit and see the effect
	fmt.Printf("State: %016b\n", f.state)
	fmt.Println("Is open:", f.isSolved())

	f.turnBit(0, 0) // Flip back the same bit
	fmt.Printf("State: %016b\n", f.state)
	fmt.Println("Is open:", f.isSolved())

	// Shuffle the fridge to get a random starting state
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(1000)
	for i := 0; i < n; i++ {
		x := rand.Intn(4)
		y := rand.Intn(4)
		f.turnBit(x, y)
	}

	fmt.Println("Shuffled state:", f.state)

	// Solve the fridge puzzle
	solution := SolveBit(&f)
	fmt.Println("Solution found:", solution)
	for _, turn := range solution {
		f.turnBit(turn[0], turn[1])
	}
	fmt.Println("Is open after applying solution:", f.isSolved())
}

type Fridge struct {
	matrix [4][4]int
}

func (f *Fridge) Turn(x, y int) {
	if x < 0 || x > 3 || y < 0 || y > 3 {
		return
	}

	for i := 0; i < 4; i++ {
		f.matrix[y][i] ^= 1
	}

	for i := 0; i < 4; i++ {
		f.matrix[i][x] ^= 1
	}

	f.matrix[y][x] ^= 1
}

func (f *Fridge) IsOpen() bool {
	for y := 0; y < 4; y++ {
		for x := 0; x < 4; x++ {
			if f.matrix[y][x] != 0 {
				return false
			}
		}
	}
	return true
}

func (f *Fridge) Copy() Fridge {
	var newFridge Fridge
	for y := 0; y < 4; y++ {
		for x := 0; x < 4; x++ {
			newFridge.matrix[y][x] = f.matrix[y][x]
		}
	}
	return newFridge
}

func (f *Fridge) Shufle() {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(1000)
	for i := 0; i < n; i++ {
		x := rand.Intn(4)
		y := rand.Intn(4)
		f.Turn(x, y)
	}
}

func (f *Fridge) Print() {
	for y := 0; y < 4; y++ {
		fmt.Println(f.matrix[y])
	}
}

func Solve(f *Fridge) ([][2]int, bool) {

	return nil, false // Решение не найдено
}

type FridgeBit struct {
	state uint16 // Using a uint16 to represent the 4x4 matrix.
}

// turnBit flips the bits in the state of the fridge at the given row and column,
// and the corresponding row and column bits.
func (f *FridgeBit) turnBit(row, col int) {
	bitMask := uint16(0b0001) << (row*4 + col)   // Bitmask for the (row, col) position
	rowMask := uint16(0b1111) << (row * 4)       // Bitmask for the entire row
	colMask := uint16(0b0001000100010001) << col // Bitmask for the entire column

	// Flip the bits for the entire row and column and the (row, col) position again
	// because it got flipped twice.
	f.state ^= rowMask | colMask
	f.state ^= bitMask
}

// isSolved checks if all bits are 0, meaning the fridge is 'open'
func (f *FridgeBit) isSolved() bool {
	return f.state == 0
}

// Solve tries to find a sequence of turns that opens the fridge.
func SolveBit(f *FridgeBit) [][]int {
	// This function will attempt to solve the puzzle by brute-forcing the solution.
	// If the fridge has a deterministic solving strategy, this should be replaced by that logic.
	var solution [][]int

	return solution
}
